// Loading Modules
const path = require('path');
const fs = require('fs');
const Papa = require('papaparse');

// Importing all the required functions
//Problem 1
const matchesPerYear = require(path.join(__dirname, '/src/server/1-matches-per-year.js'));
const problem1OutputFile = path.join(__dirname, '/src/public/output/1-matches-per-year.json');
// Problem 2
const matchesperTeamPerYear = require(path.join(__dirname, '/src/server/2-matches-won-per-team-per-year.js'));
const problem2OutputFile = path.join(__dirname, '/src/public/output/2-matches-won-per-team-per-year.json');
// Problem 3
const extraRunsPerTeamYear = require(path.join(__dirname, '/src/server/3-extra-runs-per-team-year.js'));
const problem3OutputFile = path.join(__dirname, '/src/public/output/3-extra-runs-per-team-year.json');
// Problem 4
const _10economicBowlersYear = require(path.join(__dirname, '/src/server/4-10-economic-bowler-year.js'));
const problem4OutputFile = path.join(__dirname, '/src/public/output/4-10-economic-bowler-year.json');
// Problem 5
const tossAndMatchWinners = require(path.join(__dirname, '/src/server/5-won-toss-and-match-both.js'));
const problem5OutputFile = path.join(__dirname, '/src/public/output/5-won-toss-and-match-both.json');
// Problem 6
const topPlayerOfMatchAllSeasons = require(path.join(__dirname, './src/server/6-top-player-of-the-match.js'));
const problem6OutputFile = path.join(__dirname, '/src/public/output/6-top-player-of-the-match.json');
// Problem 7
const strikeRate = require(path.join(__dirname, './src/server/7-strike-rate-batsman-for-all-seasons.js'));
const problem7OutputFile = path.join(__dirname, '/src/public/output/7-strike-rate-batsman-for-all-seasons.json');
// Problem 8
const highestTimesPlayerdismissed = require(path.join(__dirname, './src/server/8-highest-times-player-dismissed-by-other.js'));
const problem8OutputFile = path.join(__dirname, '/src/public/output/8-highest-times-player-dismissed-by-other.json');
// Problem 9
const bestEconomyBowler = require(path.join(__dirname, './src/server/9-bowler-with-best-economy-in-super-overs.js'));
const problem9OutputFile = path.join(__dirname, '/src/public/output/9-bowler-with-best-economy-in-super-overs.json');
//////////////////////////////////////////////////////////////////////////
// Finding matches.csv filePath & Reading it
const matchesPath = path.join(__dirname, '/src/data/matches.csv');
const matchesFile = fs.readFileSync(matchesPath);

// parsing matches.csv into array
const matches = Papa.parse(matchesFile.toString(), {
  header: true
}).data
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// Finding deliveries.csv filePath & Reading it
const deliveriesPath = path.join(__dirname, '/src/data/deliveries.csv');
const deliveriesFile = fs.readFileSync(deliveriesPath);

// ---------------------------------------------------------------------

// parsing deliveries.csv into array
const deliveries = Papa.parse(deliveriesFile.toString(), {
  header: true
}).data


//////////////////////////////////////////////////////////////////////////


// Ouput
// writing to file


//Problem 1
const problem1 = matchesPerYear(matches);
fs.writeFile( problem1OutputFile, JSON.stringify(problem1), (err) => {
    if (err)
      console.log(err);
    else {
      console.log(`Output 1st - File Written at ${problem1OutputFile} Successfully\n`);
    }
});


// Problem 2
const problem2 = matchesperTeamPerYear(matches);
fs.writeFile( problem2OutputFile, JSON.stringify(problem2), (err) => {
  if (err)
    console.log(err);
  else {
    console.log(`Output 2nd - File Written at ${problem2OutputFile} Successfully\n`);
  }
});


// Problem 3
const problem3 = extraRunsPerTeamYear(deliveries, matches, 2016);
fs.writeFile( problem3OutputFile, JSON.stringify(problem3), (err) => {
  if (err)
    console.log(err);
  else {
    console.log(`Output 3rd - File Written at ${problem3OutputFile} Successfully\n`);
  }
});


// Problem 4
const problem4 = _10economicBowlersYear(deliveries, matches, 2015);
fs.writeFile( problem4OutputFile, JSON.stringify(problem4), (err) => {
  if (err)
    console.log(err);
  else {
    console.log(`Output 4th - File Written at ${problem4OutputFile} Successfully\n`);
  }
});


// Problem 5
const problem5 = tossAndMatchWinners(matches);
fs.writeFile( problem5OutputFile, JSON.stringify(problem5), (err) => {
  if (err)
    console.log(err);
  else {
    console.log(`Output 5th - File Written at ${problem5OutputFile} Successfully\n`);
  }
});


// Problem 6
const problem6 = topPlayerOfMatchAllSeasons(matches);
fs.writeFile( problem6OutputFile, JSON.stringify(problem6), (err) => {
  if (err)
    console.log(err);
  else {
    console.log(`Output 6th - File Written at ${problem6OutputFile} Successfully\n`);
  }
});


// Problem 7
const problem7 = strikeRate(matches, deliveries, 'V Kohli');
fs.writeFile( problem7OutputFile, JSON.stringify(problem7), (err) => {
  if (err)
    console.log(err);
  else {
    console.log(`Output 7th - File Written at ${problem7OutputFile} Successfully\n`);
  }
});


// Problem 8
const problem8 = highestTimesPlayerdismissed(deliveries, 'DA Warner');
fs.writeFile( problem8OutputFile, JSON.stringify(problem8), (err) => {
  if (err)
    console.log(err);
  else {
    console.log(`Output 8th - File Written at ${problem8OutputFile} Successfully\n`);
  }
});


// Problem 9
const problem9 = bestEconomyBowler(deliveries);
fs.writeFile( problem9OutputFile, JSON.stringify(problem9), (err) => {
  if (err)
    console.log(err);
  else {
    console.log(`Output 9th - File Written at ${problem9OutputFile} Successfully\n`);
  }
});



// Points to keep in mind:

// Proper Formatting
// Clean Code - Final
// Commits-Properly

// Naming Variable & Functions Properly
// Hardcoding your file paths
// Use of HOF - Declarative/ Imperative Programming

// Making Functions Generic
// Function/ Module : Application of Single Responsibility Principle
// reducing Time Complexity
// map filter forEach
// Object.entries and Object.fromEntries
// KISS
