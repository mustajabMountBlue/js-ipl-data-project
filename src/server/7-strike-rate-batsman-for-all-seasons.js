module.exports = batsmanStrikeRate = (matches, deliveries, batsman) => {
  const mapIDseason = new Map();
  matches.forEach((match) => mapIDseason.set(match.id, match.season));

  const strikeRateArr = deliveries
    .filter((delivery) => delivery.batsman === batsman)
    .reduce((accu, curr) => {
      let season = mapIDseason.get(curr.match_id);
      // Destructuring for better Readability
      [batsman, totalRuns] = [curr.batsman, +curr.batsman_runs]; // Adding + to convert to int
      // Main logic goes here
      if (accu.hasOwnProperty(batsman)) {
        if (accu[batsman].hasOwnProperty(season)) {
          accu[batsman][season][0] += totalRuns; // 0 index has total runs
          accu[batsman][season][1] += 1; // 1 index has total balls
        } else {
          accu[batsman][season] = [totalRuns, 1];
        }
      } else {
        accu[batsman] = { [season]: [totalRuns, 1] };
      }

      return accu;
    }, {});

  const strikeRate = Object.entries(strikeRateArr).map((striker) => {
    // Destructuring for better Readability
    [strikerBatsman, strikeRateAllYear] = [striker[0], striker[1]];
    const allYearStrikeRate = Object.entries(strikeRateAllYear).map(
      (strikeArr) => {
        const season = strikeArr[0];
        const runs = strikeArr[1][0];
        const bowls = strikeArr[1][1];
        return { [season]: (runs * 100) / bowls };
      }
    );
    // Return arr  [ "V Kohli" , [ ["2008",strikeRate]....... ]
    return { [strikerBatsman]: allYearStrikeRate };
  });
  return strikeRate;
};
// run * 100 /bowls
