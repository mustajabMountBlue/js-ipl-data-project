module.exports = matchesPerYear = (matches) => {
  const result = matches
    .map((match) => match.season)
    .reduce(
      (accumulator, currentValue) => {
        // Main Logic Goes Here
        if (accumulator.hasOwnProperty(currentValue)) {
          accumulator[currentValue]++;
        } else {
          accumulator[currentValue] = 1;
        }
        return accumulator;
      },
      {} // Passing Accumulator as empty object
    );

  return result;
};
