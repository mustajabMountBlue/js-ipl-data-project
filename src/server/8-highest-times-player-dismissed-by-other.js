module.exports = highestTimesPLayerdismissed = (deliveries, player) => {
  const dismissedArr = deliveries
    .filter((delivery) => delivery.player_dismissed === player)
    .reduce((accu, curr) => {
      // If not dismissed
      if (!curr.player_dismissed) return accu;

      // Destructuring for better readability
      const bowler = curr.bowler;

      // Main Logic goes here
      if (accu.hasOwnProperty(player)) {
        // if dismissed player already exist

        if (accu[player].hasOwnProperty(bowler)) {
          //
          // Increase the dismissal count, if bowler already exist
          accu[player][bowler]++;
        } else {
          // Create a bowler, if not exist
          accu[player][bowler] = 1;
        }
      } else {
        accu[player] = { [bowler]: 1 };
      }

      return accu;
    }, {});

  const playerDismissed = Object.entries(dismissedArr).map(
    (dismissedPlayer) => {
      const dismissedByArr = dismissedPlayer[1];
      sortedOrder = Object.entries(dismissedByArr).sort(
        (bowlerA, bowlerB) => bowlerB[1] - bowlerA[1]
      );

      const topScore = sortedOrder[0][1];
      const topDismissedBy = sortedOrder.filter(
        (bowlers) => bowlers[1] === topScore
      );

      return topDismissedBy;
    }
  );
  return playerDismissed;
};
