module.exports = wonTossAndMatch = (matches) => {
  let result = matches.reduce((accumulator, current) => {
    if (current.toss_winner === current.winner) {
      if (accumulator.hasOwnProperty(current.winner)) {
        accumulator[current.winner]++;
      } else {
        accumulator[current.winner] = 1;
      }
    }

    return accumulator;
  }, {});

  return result;
};
