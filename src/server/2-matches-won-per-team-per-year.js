module.exports = matchesPerYearPerTeam = (matches) => {
  const result = matches
    .map((match) => [match.winner, match.season])
    .reduce(
      (accumulator, currentValue) => {
        // Destructuring for better readability
        [team, year] = [currentValue[0], currentValue[1]];

        // If NO winner was there
        if (!team) return accumulator;
        // Main Logic Goes Here
        if (accumulator.hasOwnProperty(team)) {
          // To reduce by year
          let internalAccu = accumulator[team];

          if (internalAccu.hasOwnProperty(year)) {
            internalAccu[year]++;
          } else {
            internalAccu[year] = 1;
          }
        } else {
          // Create new object
          // Object Example:
          // "Sunrisers Hyderabad":{"2013":10,"2014":6,"2015":7, .........
          accumulator[team] = { year: 1 };
          // Why we can't write this as accu[team]  = year; & accu[team][year] = 1;
        }
        return accumulator;
      },
      {} // Passing Accumulator as empty object
    );

  return result;
};
