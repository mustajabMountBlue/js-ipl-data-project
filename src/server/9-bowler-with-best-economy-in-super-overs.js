module.exports = bestEconomyBowler = (deliveries) => {
  let bowlersReducedList = deliveries.reduce((accumulator, current) => {
    if (+current.is_super_over) {
      if (accumulator.hasOwnProperty(current.bowler)) {
        accumulator[current.bowler].ball += 1;
        accumulator[current.bowler].run +=
          +current.total_runs - +current.bye_runs - +current.legbye_runs;
      } else {
        accumulator[current.bowler] = {
          ball: 1,
          run: +current.total_runs - +current.bye_runs - +current.legbye_runs,
        };
      }
    }
    return accumulator;
  }, {});

  let result = Object.entries(bowlersReducedList)
    .map((bowler) => {
      // Destructuring for better readability
      [Bowler, BowlerData] = [bowler[0], bowler[1]];
      // Calculating economy with data
      const economy = (BowlerData.run * 6) / BowlerData.ball; // bowler Data at index 1 is runs and 0 is balls

      return [Bowler, { economy: economy }];
    })
    .sort((playerA, playerB) => playerA[1].economy - playerB[1].economy);

  const bestEcoBowler = result[0];
  return bestEcoBowler;
};
