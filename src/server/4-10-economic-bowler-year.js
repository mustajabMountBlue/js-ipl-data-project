module.exports = top10economicBowlers2015 = (deliveries, matches, year) => {
  let mapping = new Map();
  // Mapping IDs for fast accessing later
  matches.forEach((match) => {
    if (match.season == year) mapping.set(match.id, 1);
  });

  const bowlers = deliveries
    .filter((delivery) => mapping.get(delivery.match_id))
    .map((delivery) => [delivery.bowler, +delivery.total_runs])
    .reduce((accumulator, current) => {
      // Destructuring for better readability
      [bowler, runs] = [current[0], current[1]];

      // Main Logic goes here
      if (accumulator.hasOwnProperty(bowler)) {
        // bowler : { // this exist }
        accumulator[bowler].runs += runs;
        accumulator[bowler].balls += 1;
      } else {
        accumulator[bowler] = { runs: current[1], balls: 1 };
      }

      return accumulator;
    }, {});
  // Bowlers Array now contains bowlers data
  /*
    {
        'UT Yadav': { runs: 387, balls: 266 },
        'M Morkel': { runs: 212, balls: 174 },
        'Shakib Al Hasan': { runs: 123, balls: 89 },
        ........
    }
    */

  const result = Object.entries(bowlers)
    // Mapping bowlers with their economy
    .map((bowlers) => [bowlers[0], (bowlers[1].runs * 6) / bowlers[1].balls])
    .sort((bowlerA, bowlerB) => {
      return bowlerA[1] - bowlerB[1];
    }) // Sorting in asc order
    .splice(0, 10); // return top 10;

  return result;
};
