module.exports = extraRunsPerTeamYear = (deliveries, matches, year) => {
  // Hash Mapping IDs
  let idMapping = new Map();
  matches.forEach((match) => {
    if (match.season == year) {
      idMapping.set(match.id, 1);
    }
  });

  // Computing extra runs
  const extraRuns = deliveries
    .filter((delivery) => idMapping.get(delivery.match_id))
    .reduce(
      (accumulator, currentValue) => {
        // Main Logic goes here
        if (accumulator.hasOwnProperty(currentValue.bowling_team)) {
          // Added + to convert to number
          accumulator[currentValue.bowling_team] += +currentValue.extra_runs;
        } else {
          accumulator[currentValue.bowling_team] = +currentValue.extra_runs;
        }

        return accumulator;
      },
      {} // Passing Accumulator as empty object
    );
  return extraRuns;
};
