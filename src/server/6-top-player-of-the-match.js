module.exports = topPlayerOfMatch = (matches) => {
  const allManOfMatch = matches
    .map((match) => [match.season, match.player_of_match])
    .reduce((accu, curr) => {
      // Destructuring for better readability
      [season, manOfMatch] = [curr[0], curr[1]];

      // Main Logic goes here
      if (accu.hasOwnProperty(season)) {
        // If season already exist
        if (accu[season].hasOwnProperty(manOfMatch)) {
          accu[season][manOfMatch]++;
        } else {
          accu[season][manOfMatch] = 1;
        }
      } else {
        accu[season] = {
          [manOfMatch]: 1,
        };
      }
      return accu;
    }, {}); // Passing empty object

  const topPlayers = Object.keys(allManOfMatch).map((key) => {
    // key, obj[key]
    let manOfMatchSorted = Object.entries(allManOfMatch[key]).sort(
      (playerA, playerB) => playerB[1] - playerA[1]
    );

    const topValue = manOfMatchSorted[0][1];
    // If there are more than one man of match in a season
    let topManOfMatch = manOfMatchSorted.filter((man) => man[1] === topValue);

    // Mapping allManOfMatch as key => season and topManOfMatch (players)
    return { [key]: topManOfMatch };
  });

  return topPlayers;
};
